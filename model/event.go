package model

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

type Event struct {
	ID           uuid.UUID       `json:"id"`
	RegisteredAt time.Time       `json:"registeredAt"`
	OccurredAt   time.Time       `json:"occurredAt"`
	SubjectIDs   []uuid.UUID     `json:"subjectIds"`
	EventType    string          `json:"eventType"`
	EventData    json.RawMessage `json:"eventData"`
}

type EventCompanyRegistrationData struct {
	ID               uuid.UUID `json:"id"`
	Rsin             string    `json:"rsin"`
	StatutionaryName string    `json:"statutionaryName"`
	OwnerID          uuid.UUID `json:"ownerId"`
	AddressID        uuid.UUID `json:"addressId"`
}

type EventCompanyRegistration struct {
	ID            uuid.UUID `json:"id"`
	RSIN          string    `json:"rsin"`
	StatutoryName string    `json:"statutoryName"`
}

type EventCompanyClosedData struct {
	ID uuid.UUID `json:"id"`
}

type EventCompanyClose struct {
	ID uuid.UUID `json:"id"`
}
