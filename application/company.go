package application

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-handels-register-interpreter/model"
)

func (app *Application) HandleCompanyRegistration(ctx context.Context, event *model.Event) error {
	data := new(model.EventCompanyRegistrationData)
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	company := model.EventCompanyRegistration{
		ID:            event.SubjectIDs[0],
		RSIN:          data.Rsin,
		StatutoryName: data.StatutionaryName,
	}

	fmt.Println("CREATE:", event.SubjectIDs[0])

	if err := app.Request(ctx, http.MethodPost, "/businesses", company, nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}

func (app *Application) HandleCompanyClose(ctx context.Context, event *model.Event) error {
	fmt.Println("Remove not implemented in backend:", event.SubjectIDs[0])

	// data := new(model.EventCompanyClosedData)
	// if err := json.Unmarshal(event.EventData, data); err != nil {
	// 	return fmt.Errorf("json unmarshal failed: %w", err)
	// }

	// company := model.EventCompanyClose{
	// 	ID: event.SubjectIDs[0],
	// }

	// if err := app.Request(ctx, http.MethodDelete, "/businesses", company, nil); err != nil {
	// 	return fmt.Errorf("request failed: %w", err)
	// }

	return nil
}
